	/**
	 *	 README - TEMA 2 POO -
	 *	
	 *	NAUM MADALIN-CONSTANTIN
	 *		323CA
	 */


	Am ales sa structurez tema in felul urmator:

- o clasa care se ocupa de citirea inputului (InputFileReader) din fisier si 
	retinerea acestuia intr-un ArrayList<String>.

- o clasa ce primeste continutul fisierului de la prima clasa si 
	il parseaza, pentru a obtine date relevante pentru starea masinii.
	Am retinut datele in campurile clasei si am numit-o MachineState.

- pentru a retine configuratiile rotoarelor si reflectoarelor, utile mai 
	departe pentru maparea corespunzatoare a literelor, am definit 
	interfata InternalConfig. Argumentul principal pentru a folosi o 
	interfata a fost acela ca are campurile static si final, potrivit 
	pentru retinerea acestor configuratii. 
	"Peste" aceasta interfata, am creat un enum (WheelType), care sa 
	retina si detalii mai complexe (precum pozitiile notch-urilor) si
	care sa faca mai usor diferenta intre rotoare si reflectoare. 

- am observat ca reflectorul si plugboardul au functionalitati, practic, 
	identice (mapeaza la fel), singura diferenta fiind ca primesc diferit 
	configuratia.
	Asa ca am creat o clasa, Mapper, care le implementeaza pe amandoua,
	diferenta fiind facuta de constructor: 
	- exemplu instantiere plugboard: Mapper pb = new Mapper(pbConfig);
	- exemplu instantiere reflector: Mapper ref = new Mapper('C');

- de asemenea, am observat ca si functionalitatea rotoarelor se aseamana foarte 
	mult cu implementarea clasei Mapper, asa ca am definit clasa Rotor ca 
	mostenind clasa Mapper si extinzandu-i functionalitatile. 

- ultimul pas a fost reprezentat de imbinarea tuturor acestor componente si 
	functionalitati pentru a lucra impreuna, conform cerintei.
	Pentru aceasta, a trebuit sa ma gandesc la cum pot modela structura de
	tip pipeline a masinii. Am avut mai multe idei, printre care: 
	- implementarea tuturor elementelor ca un lant de mosteniri;
	- folosirea design patternului Visitor, pentru ca elementele sa se poata 
		apela reciproc;
	- crearea unei clase care sa agrege elementele masinii intr-o lista - 
		ordonata conform structurii Engima - si, parcurgand lista, fiecare
		componenta sa foloseasca rezultatul returnat de vecinul ei.

	Prima varianta mi s-a parut nenaturala, incat ar fi fortat implementarea spre
	o complexitate mult mai mare si, conceptual, structura ar fi fost mai putin 
	intuitiva.

	De asemenea, folosirea patternului Visitor m-a adus cam la aceeasi concluzie: 
	complexitatea implementarii ar fi crescut (in mod inutil). Cu toate astea,
	din punct de vedere conceptual, modelul ar fi fost unul valid, doar ca 
	extinderea la mai multe rotoare ar fi fost foarte greu de facut.

	Am considerat ultima varianta ca fiind cea mai valida, deoarece a fost si 
	usor de modelat si intuitiva (fiecare element se foloseste de ceea ce a 
	primit de la vecinul sau). In plus, extinderea modelului s-a putut face 
	foarte usor.
	Astfel, in clasa Enigma, am folosit un ArrayList<Mapper>, pe care l-am 
	populat cu:
	- o instanta a plugboardului [Mapper]
	- instante a Rotorului (numar variabil, in functie de input) [extinde Mapper]
	- o instanta a reflectorului [Mapper]

	Fiecare litera primita ca input pentru a fi codificata parcurge acea lista de 
	componente, fiind procesata de fiecare in parte, intrucat la final este 
	codificata conform cerintei.
	
	Creand clasa Enigma a permis ascunderea multor detalii legate de implementare,
	din perspectiva utilizatorului. Asta a facut ca un nivel destul de bun de 
	abstractizare sa fie atins, lucru ce a fost solicitat si prin cerinta.

