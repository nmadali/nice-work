import java.util.ArrayList;

public class Enigma {
	private MachineState state;
	private ArrayList<Mapper> machine;

	
	/**
	 * Enigma's constructor
	 */
	Enigma(MachineState ms) {

		state = ms;
		machine = new ArrayList<Mapper>();
		
		// add the Plugboard
		machine.add(new Mapper(state.getPlugboardConfig()));
		
		// add the wheels (rotors)
		for (int i = state.getNumWheels(); i >= 1; i--) {
			machine.add(new Rotor(
					state.getWheelTypes().get(i - 1), 
					state.getRingsPositions().get(i - 1), 
					state.getWheelsPositions().get(i - 1)
					));
		}
		
		// add Reflector
		machine.add(new Mapper(state.getReflectorType()));
	}

	
	/**
	 * Encrypts the message found in the input string
	 * 
	 * @return encrypted string
	 */
	String doTheThing() {
		String toReturn = "";
		char aux = '\u0000';
		for (int i = 0; i < state.message().length(); i++) {
			// plugboard
			aux = (machine.get(0)).map(state.message().charAt(i)); 
			
			// rotate first wheel
			int ok = rotateFirstWheel();
			
			// direct mapping
			for (int j = 1; j <= state.getNumWheels(); j++) { 
				aux = ((Rotor) machine.get(j)).map(aux);
				((Rotor) machine.get(j)).changeEncryptionMode();
			}

			// reflector
			aux = machine.get(state.getNumWheels() + 1).map(aux); 
			
			// reverse mapping
			for (int j = state.getNumWheels(); j >= 1; j--) {
				aux = ((Rotor) machine.get(j)).map(aux);
				((Rotor) machine.get(j)).changeEncryptionMode();
			}
			
			// plugboard
			aux = machine.get(0).map(aux); 
			
			// double-step
			if (ok == 1) doubleStepping();
			toReturn += aux;
		}
		return toReturn;
	}
	
	
	private int rotateFirstWheel() {
		((Rotor)machine.get(1)).rotate();
		for (char notch : ((Rotor)machine.get(1)).getNotch()) {
			if (notch == ((Rotor)machine.get(1)).getWheelPos()) {
				((Rotor)machine.get(2)).rotate();
				break;
			}
		}
		for (char notch : ((Rotor)machine.get(2)).getNotch()) {
			if ((char)(notch - 1) == ((Rotor)machine.get(2)).getWheelPos()) {
				return 1;
			}
		}
		return 0;
	}
	
	
	private void doubleStepping() {
		((Rotor)machine.get(2)).rotate();
		((Rotor)machine.get(3)).rotate();
	}
}
