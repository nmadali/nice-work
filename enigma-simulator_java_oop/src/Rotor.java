import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Implements the functionalities of the wheels. 
 * Extends Mapper.
 * 
 * @author Madalin Naum - 323CA
 */
public class Rotor extends Mapper {
	private char wheelPos;
	private char ringPos;
	private int encryptionMode; // 0 for direct encryption, 1 for reverse
	private char[] notch;
	/** 
	 * possible configs for the rotors(wheels) 
	 */
	private static final Map<Integer, WheelType> rotMap = new 
			HashMap<Integer, WheelType>();
	static {
		rotMap.put(1, WheelType.ROTOR1);
		rotMap.put(2, WheelType.ROTOR2);
		rotMap.put(3, WheelType.ROTOR3);
		rotMap.put(4, WheelType.ROTOR4);
		rotMap.put(5, WheelType.ROTOR5);
		rotMap.put(6, WheelType.ROTOR6);
		rotMap.put(7, WheelType.ROTOR7);
		rotMap.put(8, WheelType.ROTOR8);
	}

	
	/**
	 * Explicit Constructor
	 */
	Rotor(int wheelType, char ringPos, char wheelPos) {
		this.ringPos = ringPos;
		this.wheelPos = wheelPos;
		this.internalConfig = new ArrayList<String>(
				Arrays.asList(rotMap.get(wheelType).getConfig()));
		this.encryptionMode = 0;
		this.notch = rotMap.get(wheelType).notch;
	}
	
	
	/**
	 * Implicit Constructor
	 */
	Rotor() {
		super();
	}
	
	
	/**
	 * Encodes the letter as it should be encoded, according 
	 * to the internal config of the wheel and the offset 
	 * between ring's and wheel's position.
	 * 
	 * @param letter [char]
	 * @param mode [int] -> direct encoding (0) or reverse encoding(1);
	 */
	@Override
	public char map(char letter) {
		Character.toUpperCase(letter);
		char newLetter = processLetter(letter, 0);
		newLetter = encode(newLetter);
		return processLetter(newLetter, 1);
	}


	private char processLetter(char letter, int mode) {
		int offset = (int) wheelPos - (int) ringPos;
		if (offset < 0)  offset += 26;
		if (offset > 26) offset -= 26;

		char processed = '\u0000';
		if (mode == 0)
			processed = (char) ((int) letter + offset);
		if (mode == 1)
			processed = (char) ((int) letter - offset);
		if (processed < (char) 65) processed += (char) 26;
		if (processed > (char) 90) processed -= (char) 26;
		return processed;
	}
	
	
	private char encode(char letter) {
		for (String s : internalConfig()) {
			if (s.charAt(encryptionMode) == letter)
				return s.charAt((encryptionMode + 1) % 2);
		}
		return letter;
	}

	
	/** Rotate the wheel */
	void rotate() {
		wheelPos++;
		if (wheelPos > 'Z') {
			wheelPos -= 26;
		}
	}

	
	/** 
	 * set encryptionMode: 0 for direct, 1 for reverse
	 */
	void changeEncryptionMode() {
		encryptionMode = (1 + encryptionMode) % 2;
	}

	
	private ArrayList<String> internalConfig() {
		return internalConfig;
	}
	
	
	int getMode() {
		return this.encryptionMode;
	}
	
	
	char getRingPos() {
		return ringPos;
	}
	
	
	char getWheelPos() {
		return wheelPos;
	}
	
	
	char[] getNotch() {
		return notch;
	}
}
