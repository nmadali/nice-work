
/**
 * Memorizes the mapping that takes place in each rotor's kernel. It also holds
 * the mapping for the reflector types.
 * 
 * @author Madalin Naum - 323CA
 * 
 */
interface InternalConfig {
	
	String[] ROT1 = {"AE", "BK", "CM", "DF", "EL", "FG", "GD", "HQ",
			"IV", "JZ", "KN", "LT", "MO", "NW", "OY", "PH", "QX", 
			"RU","SS", "TP", "UA", "VI", "WB", "XR", "YC", "ZJ"};
	
	String[] ROT2 = {"AA", "BJ", "CD", "DK", "ES", "FI", "GR", "HU",
			"IX",  "JB", "KL", "LH", "MW", "NT", "OM", "PC", "QQ",
			"RG", "SZ", "TN", "UP", "VY", "WF", "XV", "YO", "ZE"};
	
	String[] ROT3 = {"AB", "BD", "CF", "DH", "EJ", "FL", "GC", "HP", 
			"IR", "JT", "KX", "LV", "MZ", "NN", "OY", "PE", "QI", 
			"RW", "SG", "TA", "UK", "VM", "WU", "XS", "YQ", "ZO"};
	
	String[] ROT4 = {"AE", "BS", "CO", "DV", "EP", "FZ", "GJ", "HA",
			"IY","JQ", "KU", "LI", "MR", "NH", "OX", "PL", "ON", 
			"RF", "ST", "TG", "UK", "VD", "WC", "XM", "YW", "ZB"};
	
	String[] ROT5 = {"AV", "BZ", "CB", "DR", "EG", "FI", "GT", "HY",
			"IU", "JP", "KS", "LD", "MN", "NH", "OL", "PX", "QA", 
			"RW", "SM", "TJ", "UQ", "VO", "WF", "XE", "YC", "ZK"};
	
	String[] ROT6 = {"AJ", "BP", "CG", "DV", "EO", "FU", "GM", "HF",
			"IY", "JQ", "KB", "LE", "MN", "NH", "OZ", "PR", "QD",
			"RK", "SA", "TS", "UX", "VL", "WI", "XC", "YT", "ZW"};
	
	String[] ROT7 = {"AN", "BZ", "CJ", "DH", "EG", "FR", "GC", "HX",
			"IM", "JY", "KS", "LW", "MB", "NO", "OU", "PF", "QA",
			"RI", "SV", "TL", "UP", "VE", "WK", "XQ", "YD", "ZT"};

	String[] ROT8 = {"AF", "BK", "CQ", "DH", "ET", "FL", "GX", "HO",
			"IC", "JB", "KJ", "LS", "MP", "ND", "OZ", "PR", "QA",
			"RM", "SE", "TW", "UN", "VI", "WU", "XY", "YG", "ZV"};

	String[] REF_B = { "AY", "BR", "CU", "DH", "EQ", "FS", "GL", "IP", "JX",
			"KN", "MO", "TZ", "VW" };
	
	String[] REF_C = { "AF", "BV", "CP", "DJ", "EI", "GO", "HY", "KR", "LZ",
			"MX", "NW", "TQ", "SU" };
}

/**
 * This enum holds information about each wheel type (the kind of
 * wiring it has and which are the notches).
 * 
 */
public enum WheelType {
	
	ROTOR1(InternalConfig.ROT1, new char[] { 'R' }), 
	ROTOR2(InternalConfig.ROT2, new char[] { 'F' }), 
	ROTOR3(InternalConfig.ROT3, new char[] { 'W' }), 
	ROTOR4(InternalConfig.ROT4, new char[] { 'K' }), 
	ROTOR5(InternalConfig.ROT5, new char[] { 'A' }), 
	ROTOR6(InternalConfig.ROT6, new char[] { 'A', 'N' }), 
	ROTOR7(InternalConfig.ROT7, new char[] { 'A', 'N' }), 
	ROTOR8(InternalConfig.ROT8, new char[] { 'A', 'N' }), 
	REF_B(InternalConfig.REF_B), 
	REF_C(InternalConfig.REF_C);

	String[] internalConfig;
	char[] notch;

	/**
	 * Getter for internalConfig
	 */
	String[] getConfig() {
		return internalConfig;
	}

	/**
	 * Constructor for wheel type
	 */
	WheelType(String[] rotConf, char[] notch) {
		this.internalConfig = rotConf;
		this.notch = notch;
	}

	/**
	 * Constructor for reflector type
	 */
	WheelType(String[] refConf) {
		this.internalConfig = refConf;
	}
}