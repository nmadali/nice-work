import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


/**
 * Reads every line from the input file.
 * 
 * @author Naum Madalin - 323CA
 *
 */
public class InputFileReader {
	private String filePath;

	InputFileReader(String path) {
		this.filePath = path;
	}

	/**
	 * Reads every line from the input file. Returns them as an 
	 * ArrayList of Strings.
	 * 
	 */
	public ArrayList<String> readFile() throws IOException {
		// open file and buffer for reading
		FileReader fr = new FileReader(filePath);
		BufferedReader bfr = new BufferedReader(fr);

		ArrayList<String> fileContent = new ArrayList<String>();
		String line = null;

		// read every line
		while ((line = bfr.readLine()) != null) {
			fileContent.add(line);
		}

		// close the buffer
		bfr.close();

		// return
		return fileContent;
	}
}