import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Mapper implements the functionality for the Plugboard and the Reflector.
 * Mapper also is the parent of Rotor.
 * 
 * @author Madalin Naum - 323CA
 * 
 */
public class Mapper {
	protected ArrayList<String> internalConfig;
	/** 
	 * possible configurations for reflectors 
	 */
	protected static final Map<Character, WheelType> refMap = new 
			HashMap<Character, WheelType>();
	static {
		refMap.put('B', WheelType.REF_B);
		refMap.put('C', WheelType.REF_C);
	}
	

	/**
	 * Constructor used for Plugboard
	 */
	Mapper(ArrayList<String> config) {
		this.internalConfig = config;
	}
	
	/**
	 * Constructor used for Reflector
	 * 
	 * @param type [char]
	 */
	Mapper(char type) {
		Character.toUpperCase(type);
		this.internalConfig = new ArrayList<String>(
					Arrays.asList(refMap.get(type).getConfig()));
	}
	
	/**
	 * Implicit constructor
	 */
	Mapper() {
	}

	/**
	 * Maps letter according to the internal config. 
	 * Used by Reflector & Plugboard.
	 * 
	 * @param letter
	 * @return char
	 */
	public char map(char letter) {
		Character.toUpperCase(letter);
		for (String s : internalConfig) {
				if (s.contains(String.valueOf(letter)))
					return s.charAt((s.indexOf(letter) + 1) % 2);
			}
		return letter;
	}
}
