import java.io.IOException;
import java.util.ArrayList;

public class Main {
	public static void main(String[] args) throws IOException {
		String filename = args[0];
		ArrayList<String> fileContent = null;

		InputFileReader file = new InputFileReader(filename);
		fileContent = file.readFile();

		MachineState ms = new MachineState();
		ms.getMachineState(fileContent);
		
		Enigma enigma = new Enigma(ms);
		System.out.println(enigma.doTheThing());
	}
}