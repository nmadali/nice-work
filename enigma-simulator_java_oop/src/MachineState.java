import java.util.ArrayList;

/**
 * Parses the machine's state from the content of the file.
 * 
 * @author Naum Madalin - 323CA
 * 
 */
public class MachineState {
	private String alphabet;
	private ArrayList<String> plugboardConfig;
	private char reflectorType;
	private ArrayList<Integer> wheelTypes;
	private ArrayList<Character> ringsPositions;
	private ArrayList<Character> wheelsPositions;
	private String message;
	private int numWheels;

	
	/**
	 * Constructor
	 */
	MachineState() {

		plugboardConfig = new ArrayList<String>();
		wheelTypes = new ArrayList<Integer>();
		ringsPositions = new ArrayList<Character>();
		wheelsPositions = new ArrayList<Character>();
	}

	/**
	 * Parses the input file content and sets the initial state of the Machine.
	 */
	void getMachineState(ArrayList<String> fileContent) {

		// reset the state
		resetState();
		// start parsing
		alphabet = fileContent.get(0).toUpperCase();
		String conf = fileContent.get(1);
		for (int i = 0; i < conf.length(); i++) {
			if (conf.substring(i, i + 1).equals("(")) {
				plugboardConfig.add(conf.substring(i + 1, i + 3));
			}
		}
		String types = fileContent.get(2).substring(0,
				fileContent.get(2).length());
		reflectorType = types.charAt(0);
		// parse wheel types
		char[] aux = types.toCharArray();
		for (int i = 1; i < aux.length; i++) {
			if (aux[i] != ' ') {
				wheelTypes.add(Character.getNumericValue(aux[i]));
			}
		}
		// parse rings positions
		char[] ringPosHelper = new char[aux.length];
		fileContent.get(3).getChars(0, fileContent.get(3).length(),
				ringPosHelper, 0);
		for (char c : ringPosHelper) {
			if (alphabet.indexOf(c) != -1)
				ringsPositions.add(c);
		}
		// parse wheels positions
		char[] wheelsPosHelper = new char[aux.length];
		fileContent.get(4).getChars(0, fileContent.get(4).length(),
				wheelsPosHelper, 0);
		for (char c : wheelsPosHelper) {
			if (alphabet.indexOf(c) != -1)
				wheelsPositions.add(c);
		}
		numWheels = wheelsPositions.size();
		for (int i = 6; i < fileContent.size(); i++) {
			message += fileContent.get(i).toUpperCase();
		}
	}

	private void resetState() {
		alphabet = "";
		plugboardConfig.clear();
		reflectorType = '\u0000';
		wheelTypes.clear();
		ringsPositions.clear();
		wheelsPositions.clear();
		message = "";
	}

	/** 
	 * Returns the message to be encoded/decoded. 
	 */
	String message() {
		return message;
	}

	
	char getReflectorType() {
		return reflectorType;
	}

	
	ArrayList<String> getPlugboardConfig() {
		return plugboardConfig;
	}

	
	ArrayList<Integer> getWheelTypes() {
		return wheelTypes;
	}

	
	ArrayList<Character> getRingsPositions() {
		return ringsPositions;
	}

	
	ArrayList<Character> getWheelsPositions() {
		return wheelsPositions;
	}

	
	int getNumWheels() {
		return numWheels;
	}
}