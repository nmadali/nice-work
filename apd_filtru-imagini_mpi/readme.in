===============================================================================
							NAUM MADALIN-CONSTANTIN
								GRUPA 333CA
								 	- 
								 TEMA 3 APD
===============================================================================

	Continut:
	--------

1.	Detaliile temei
2.	Learning benefits
3. 	Probleme de implementare
4. 	Detaliile implementarii
5.	Multumiri & Concluzii

===============================================================================

	Detaliile temei
	---------------

	Tema a presupus implementarea unui program MPI care sa poata procesa 
imagini .pgm, aplicand un anumit filtru de convolutie pe ele (sobel sau
mean_removal).

	Dificultatea temei a constat in special in implementarea paradigmei
Heartbeat si respectarea modului de comunicare in topologia arborescenta
de procese.

	Threadul cu rank == 0 a fost considerat radacina a arborelui. Am ales sa
implementez algoritmii (atat pentru radacina cat si pentru celelalte
thread-uri) in functia main(), dupa modelul standard:
	if (rank == 0) {
		// algoritm radacina
	}
	else {
		// algoritm noduri
	}

===============================================================================

	Learning benefits
	-----------------

	Recunosc ca dupa prima citire a temei am intrat in panica. Asta pentru ca
la tema2 am intampinat niste bug-uri extrem de ciudate si, initial, sobel, 
mean_removal si MPI pareau foarte complexe. 

	Depinzand de aceasta tema pentru a nu trece la taxa, credeam ca voi
avea probleme, insa am recitit enuntul, am cautat cateva explicatii pe 
internet si pe forum si lucrurile au inceput sa se lege.

	Acum am un sentiment de satisfactie si incredere sporita, pentru ca 
am simtit tema ca o provocare peste care am reusit sa trec cu succes. 

	Nu pot spune ca a fost o tema extrem de complicata, insa a fost una 
care a necesitat putere de concentrare si meticulozitate pentru a putea
aplica cele invatate.

	Overall, pot spune ca m-am acomodat mult mai bine cu MPI si cu conceptul
de programare distribuita, intelegandu-i modul de lucru si utilitatea. Este
de mentionat si faptul ca mi-a oferit satisfactie sa pot observa rezultatul 
final (imaginile procesate corect).

===============================================================================
	
	Detaliile implementarii
	-----------------------

	Nu am intampinat probleme majore pe parcursul implementarii. Majoritatea
erorilor au fost cauzate de greseli datorate lipsei de concentrare.

	A existat o situatie pe care o consider relevant de mentionat: 
- am primit seg fault la alocarea statica unei matrici (int a[linii][coloane];)
	
	Motivul seg fault-ului era ca dimensiunile matricii depaseau spatiul 
alocat pentru stiva thread-ului in cauza. Rezolvarea a fost alocarea dinamica,
matricea fiind stocata pe heap.

_____
	Descrierea algoritmului:
- se initializeaza thread-urile MPI
- thread-urile citesc liniile alocate lor din fisierul topologie

Algoritmul radacinii:
- thread-ul 0 parseaza informatiile despre imagini si initializeaza algoritmul
	- initiaza totul prin a trimite propriul rank catre copii
		- copii repeta actiunea, astfel ca toate nodurile din arbore isi afla
				parintii
	- parseaza fiecare imagine primita ca input intr-o matrice
	- sparge matricea in blocuri pe care le trimite la copii
	- asteapta sa primeasca blocurile de pixeli, procesate de frunze
	- cand le primeste le scrie in fisier in format .pgm
	- repeta procesul pentru toate imaginile
	- cere statisticile frunzelor si le scrie in fisier.

Algoritmul celorlalte thread-uri:
- primul mesaj primit este rank-ul parintelui, pentru a memora legatura.
	- fiecare nod trimite rank-ul sau copiilor
- nodurile primesc si dau mai departe numarul imaginilor ce trebuie procesate
- pentru fiecare imagine, asteapta blocul de matrice atribuit lor
	- daca sunt frunze, il proceseaza.
	- daca nu, il sparg in mai multe bucati si-l trimit la copii
- cand o frunza a procesat un bloc, il trimite la parinte
- parintele il trimite mai sus in structura, pana ajunge la radacina
	(am considerat redundant sa concatenez matricile primite de la copii,
		pentru ca ele sunt primite in ordinea in care trebuie scrise in
		fisier; ele doar sunt trimise pana la radacina, iar aceasta le scrie,
		fara a fi concatenate in matrici)
- la primirea flagului de terminare, frunzele trimit statisticile catre
	radacina. Celelalte noduri propaga informatiile corecte.
- algoritmul se repeta.


===============================================================================

	Multumiri & concluzii
	---------------------

	Multumesc pentru oportunitatea de invatare!

	Am incercat sa implementez tema cat de bine am putut, pentru ca am nevoie
de maxim sa pot intra in examen. Sper ca este in regula. Multumesc!

===============================================================================
