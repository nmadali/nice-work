#include <mpi.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>

using namespace std;

#define ERROR 1
#define SUCCESS 0
#define SOBEL -1
#define MEAN_REMOVAL -2 
#define NO_PARENT -1
#define EXTENSION 2
#define TERMINATION_FLAG -7
#define DEFAULT_TAG 1

// sobel filter & values
const int sobel_x[3][3] = { 
	1, 0, -1,
	2, 0, -2,
	1, 0, -1 };
const int sobel_factor = 1;
const int sobel_displacement = 127;

// mean_removal filter & values
const int mean_removal[3][3] = { 
	-1, -1, -1,
	-1,  9, -1 ,
	-1, -1, -1 };
const int mean_factor = 1;
const int mean_displacement = 0;

// opens file for reading
void open_file(ifstream& file, string addr) {
	file.open(addr.c_str());

	if (!file) {
	    cerr << "Unable to open file: " << addr << endl;                            
	    exit(ERROR);
	}
}

// opens file for writing
void open_file(ofstream& file, string addr) {
	file.open(addr.c_str());

	if (!file) {
	    cerr << "Unable to open file: " << addr << endl;
	    exit(ERROR);
	}
}

// reads the adjacency matrix from file (topologie.in)
void parse_children(int num_workers, int rank, ifstream& topologie, 
					vector<int>* children) {
	int temp;
	string line[num_workers];

	// for each worker
	for (int worker = 0 ; worker < num_workers; worker++) {
		// each rank reads only his part
		if (rank == worker) {
			// ignore the reduntant lines
			for (int i = 0 ; i < rank ; i++) {
				getline(topologie, line[rank]);
			}
			// worker reads his attributed line
			getline(topologie, line[rank]);

			// parse line to get children
			replace(line[rank].begin(), line[rank].end(), ':', ' ');
			stringstream ss(line[rank]);
			while (ss >> temp) {
				children[rank].push_back(temp);
			}
			children[rank].erase(children[rank].begin());
		}
	}
}

// prints the adjacency matrix of the threads
void print_children(vector<int>* children, int rank) {
	cout << "rank " << rank << ": ";
	for (int j = 0 ; j < children[rank].size(); j++) 
		cout << children[rank][j] << " ";
	cout << endl;
}

struct image {
	string filter;
	int tag;
	string unfiltered_img;
	string filtered_img;
};

// parse the info about each image from imagini.in 
void parse_img_info(int num_pictures, ifstream& pictures, vector<struct image>& img_info) {
	string lines[num_pictures];
	stringstream ss[num_pictures];
	string empty_line;

	// for each picture
	for (int i = 0 ; i <= num_pictures; i++) {
		// this will be empty
		if (i == 0) {
			getline(pictures, empty_line);
			continue;
		}

		// read each line into lines[num_pictures]
		getline(pictures, lines[i - 1]);
		ss[i - 1].str(lines[i - 1]);

		// parse input into struct image img_info
		struct image img;
		ss[i - 1] >> img.filter;
		ss[i - 1] >> img.unfiltered_img;
		ss[i - 1] >> img.filtered_img;

		// set filter tag
		(img.filter.compare("sobel") == 0) ? img.tag = SOBEL : img.tag = MEAN_REMOVAL;
		img_info.push_back(img);
	}
}

void print_img_info(vector<struct image>& img_info, int num_pictures) {
	for (int i = 0 ; i < num_pictures ; i++) {
		cout << img_info[i].filter << endl;
		cout << img_info[i].tag << endl;
		cout << img_info[i].unfiltered_img << endl;
		cout << img_info[i].filtered_img << endl << endl;
	}
}

void parse_img_matrix(ifstream& image, int max, int rows, int columns, int** img_matrix) {
	// ignore first two lines
	rows += EXTENSION;
	columns += EXTENSION;

	// read each pixel
	int pixel;
	for (int r = 1 ; r < rows - 1; r++) {
		for (int c = 1 ; c < columns - 1; c++) {
			image >> pixel;
			// verify if pixel's value is in the correct range
			if (pixel < 0) img_matrix[r][c] = 0;
			else if (pixel > max) img_matrix[r][c] = max;
			else img_matrix[r][c] = pixel;
		}
	}
}

// verifies if some thread is leaf or not
int is_leaf(int rank, vector<int> children[]) {
	return (children[rank].size() == 0);
}

// creates new matrix with 0 margins to each row/column
int** create_extended_matrix(int rows, int columns) {
	rows += 2;
	columns += 2;

	// create matrix
	int** img_matrix = new int*[rows];
	for (int j = 0 ; j < rows; j++) {
		img_matrix[j] = new int[columns];
	}

	// bordare cu 0
	for (int j = 0; j < columns; j++) {
		img_matrix[0][j] = 0;
		img_matrix[rows - 1][j] = 0;
	}

	for (int j = 0; j < rows; j++) {
		img_matrix[j][0] = 0;
		img_matrix[j][columns - 1] = 0;
	}

	return img_matrix;
}

// computes new pixel values after applying sobel
void apply_sobel_x(int **piece, int rows, int columns, int **new_mat, int max_value) {
	// apply Sobel_X
	for (int j = 1; j < rows + 1; j++) {
		for (int k = 1; k < columns + 1; k++) {
			new_mat[j - 1][k - 1] = 
					piece[j - 1][k - 1] 	* 	sobel_x[0][0] +
					piece[j - 1][k] 		* 	sobel_x[0][1] +
					piece[j - 1][k + 1] 	* 	sobel_x[0][2] +
					piece[j][k - 1] 		* 	sobel_x[1][0] +
					piece[j][k] 			* 	sobel_x[1][1] +
					piece[j][k + 1] 		* 	sobel_x[1][2] +
					piece[j + 1][k - 1] 	* 	sobel_x[2][0] +
					piece[j + 1][k] 		* 	sobel_x[2][1] +
					piece[j + 1][k + 1] 	* 	sobel_x[2][2];
			new_mat[j - 1][k - 1] /= sobel_factor;
			new_mat[j - 1][k - 1] += sobel_displacement;
			if (new_mat[j - 1][k - 1] > max_value) new_mat[j - 1][k - 1] = max_value;
			if (new_mat[j - 1][k - 1] < 0) new_mat[j - 1][k - 1] = 0;
		}
	}
}

// computes new pixel values after applying mean_removal
void apply_mean_removal(int **piece, int rows, int columns, int **new_mat, int max_value) {
	// apply Sobel_X
	for (int j = 1; j < rows + 1; j++) {
		for (int k = 1; k < columns + 1; k++) {
			new_mat[j - 1][k - 1] = 
					piece[j - 1][k - 1] 	* 	mean_removal[0][0] +
					piece[j - 1][k] 		* 	mean_removal[0][1] +
					piece[j - 1][k + 1] 	* 	mean_removal[0][2] +
					piece[j][k - 1] 		* 	mean_removal[1][0] +
					piece[j][k] 			* 	mean_removal[1][1] +
					piece[j][k + 1] 		* 	mean_removal[1][2] +
					piece[j + 1][k - 1] 	* 	mean_removal[2][0] +
					piece[j + 1][k] 		* 	mean_removal[2][1] +
					piece[j + 1][k + 1] 	* 	mean_removal[2][2];
			new_mat[j - 1][k - 1] /= mean_factor;
			new_mat[j - 1][k - 1] += mean_displacement;
			if (new_mat[j - 1][k - 1] > max_value) new_mat[j - 1][k - 1] = max_value;
			if (new_mat[j - 1][k - 1] < 0) new_mat[j - 1][k - 1] = 0;
		}
	}
}

// writes new pixel values into file
void write_results_to_file(ofstream& out, int rows, int columns, int** processed_mat) {
	// print results in the file
	for (int i = 0; i < rows; i++) {
		for (int j = 0 ; j < columns; j++) {
			out << processed_mat[i][j] << endl;
		}
	}
}

// writes retained header_lines into file
void write_header_lines(ofstream& out, vector<string> header_lines) {
	for (int i = 0; i < header_lines.size(); i++) {
		out << header_lines[i] << endl;
	}
}

int main (int argc, char** argv) {
	int rank, num_workers;

	if (argc != 4) {
		cerr << "Incorrect number of command line arguments" << endl;
		exit(-1);
	}

	// initialize MPI threads
	MPI_Init(&argc, &argv);
	MPI_Status status;
	MPI_Request request;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_workers);

	ifstream topologie;
	open_file(topologie, argv[1]);

	// parse children from topologie.in
	vector<int> children[num_workers];
	parse_children(num_workers, rank, topologie, children);

	int parent_of[num_workers];
	int rows[num_workers];
	int columns[num_workers];
	int statistics[num_workers];

	vector<struct image> img_info;
	vector<string>  header_lines;

	// root algorithm
	if (rank == 0) {
		parent_of[0] = NO_PARENT;
		for (int i = 0 ; i < num_workers; i++) statistics[i] = 0;

		ifstream pictures;
		ofstream stats;
		open_file(pictures, argv[2]);
		open_file(stats, argv[3]);

		int num_pictures;
		pictures >> num_pictures;

		// rank 0 reads info about each image into img_info
		parse_img_info(num_pictures, pictures, img_info);

		// sends his rank to all his children -> initiates communication
		for (int conn = 0; conn < children[0].size(); conn++) {
			MPI_Send(&rank, 1, MPI_INT, children[rank][conn], DEFAULT_TAG, 
							MPI_COMM_WORLD);
			MPI_Send(&num_pictures, 1, MPI_INT, children[rank][conn], DEFAULT_TAG, 
							MPI_COMM_WORLD);
		}

		// rank 0 parses each image
		for (int picture = 0 ; picture < num_pictures; picture++) {
			ifstream image;
			ofstream out;
			open_file(image, img_info[picture].unfiltered_img);
			open_file(out, img_info[picture].filtered_img);

			header_lines.clear();
			string dimensions;
			// ignore first two lines
			getline(image, dimensions);
			header_lines.push_back(dimensions);
			getline(image, dimensions);
			header_lines.push_back(dimensions);
			// read matrix dimensions
			getline(image, dimensions);
			header_lines.push_back(dimensions);

			stringstream ss;
			ss.str(dimensions);
			ss >> columns[rank] >> rows[rank];

			// read maximum value for pixels
			string max;
			getline(image, max);
			header_lines.push_back(max);
			int max_pixel_value = atoi(max.c_str());

			// create matrix with +2 dimensions
			int** img_matrix = create_extended_matrix(rows[rank], columns[rank]);

			parse_img_matrix(image, max_pixel_value, rows[rank], columns[rank], img_matrix);

			int rows_to_send, rows_to_recv;
			int curr_line_to_send = 1;
			int ** processed_mat;
			int var;

			// send pieces to children
			for (int child = 0; child < children[rank].size(); child++) {
				// if there are less lines than children
				if (rows[rank] < children[rank].size())
					// send each children only one line
					if (child + 1 <= rows[rank]) rows_to_send = 1;
					// if all lines are sent
					else rows_to_send = 0;
				// if there are more lines than children
				else {
					rows_to_send = int (rows[rank] / children[rank].size());
					// add remainder if it's the last children
					if (child == children[rank].size() - 1) 
						 rows_to_send += rows[rank] % children[rank].size();
				}

				// sends type of filter to all his children
				MPI_Send(&img_info[picture].tag, 1, MPI_INT, children[rank][child], 
								DEFAULT_TAG, MPI_COMM_WORLD);
				// sends max_pixel_value to all his children
				MPI_Send(&max_pixel_value, 1, MPI_INT, children[rank][child], 
								DEFAULT_TAG, MPI_COMM_WORLD);
				// sends number of rows to all his children
				MPI_Send(&rows_to_send, 1, MPI_INT, children[rank][child], 
								DEFAULT_TAG, MPI_COMM_WORLD);

				// if there are lines to send, send the matrix
				if (rows_to_send != 0) {
					// sends number of columns to all his children
					MPI_Send(&columns[rank], 1, MPI_INT, children[rank][child], 
								DEFAULT_TAG, MPI_COMM_WORLD);

					// send matrix row by row
					for (int k = 0 ; k < rows_to_send + EXTENSION; k++) {
						MPI_Send(&img_matrix[curr_line_to_send - 1][0], columns[rank] + EXTENSION, MPI_INT,
									children[rank][child], DEFAULT_TAG, MPI_COMM_WORLD);
						curr_line_to_send++;
					}
					curr_line_to_send -= EXTENSION;

					// create matrix dimensions
					rows_to_recv = rows_to_send;
					int** processed_mat = new int*[rows_to_recv];
					for (int i = 0 ; i < rows_to_recv; i++) {
						processed_mat[i] = new int[columns[rank]];
					}

					// receives filtered matrix
					for (int k = 0; k < rows_to_recv; k++) {
						MPI_Recv(&processed_mat[k][0], columns[rank], MPI_INT, 
									children[rank][child], DEFAULT_TAG, MPI_COMM_WORLD, &status);
					}

					if (child == 0) write_header_lines(out, header_lines);
					write_results_to_file(out, rows_to_recv, columns[rank], processed_mat);
				}
			}
			// close file
			out.close();
			image.close();
		}

		// rank 0 requests, receives and writes statistics to file
		int term = TERMINATION_FLAG;
		int received_stats[num_workers];
		// receives stats from children
		for (int child = 0; child < children[rank].size(); child++) {
			MPI_Send(&term, 1, MPI_INT, children[rank][child], DEFAULT_TAG, MPI_COMM_WORLD);
			MPI_Recv(&received_stats, num_workers, MPI_INT, children[rank][child], 
							DEFAULT_TAG, MPI_COMM_WORLD, &status);
			// processes its statistics
			for (int i = 0 ; i < num_workers; i++)
				if (received_stats[i] > statistics[i]) statistics[i] = received_stats[i];
		}

		// write to file
		for (int i = 0 ; i < num_workers; i++) {
			stats << i << ": " << statistics[i] << endl;
		}

		// close file
		stats.close();
	}
	else {
		for (int i = 0 ; i < num_workers; i++) statistics[i] = 0;

		// each thread receives its' parent rank
		MPI_Recv(&parent_of[rank], 1, MPI_INT, MPI_ANY_SOURCE, DEFAULT_TAG, MPI_COMM_WORLD, &status);

		// receives number of images to be processed
		int num_pictures;
		MPI_Recv(&num_pictures, 1, MPI_INT, parent_of[rank], DEFAULT_TAG, MPI_COMM_WORLD, &status);

		for (int conn = 0; conn < children[rank].size(); conn++) {
			if (children[rank][conn] != parent_of[rank]) {
				MPI_Send(&rank, 1, MPI_INT, children[rank][conn], DEFAULT_TAG, MPI_COMM_WORLD);
				MPI_Send(&num_pictures, 1, MPI_INT, children[rank][conn], DEFAULT_TAG, MPI_COMM_WORLD);
			}
		}

		// remove parent node from children list
		for (int conn = 0; conn < children[rank].size(); conn++) {
			if (children[rank][conn] == parent_of[rank]) {
				children[rank].erase(children[rank].begin() + conn);
			}
		}

		for (int picture = 0 ; picture < num_pictures; picture++) {
			// receives type of filter that must be applied
			int filter;
			MPI_Recv(&filter, 1, MPI_INT, parent_of[rank], DEFAULT_TAG, MPI_COMM_WORLD, &status);

			int max_pixel_value;
			MPI_Recv(&max_pixel_value, 1, MPI_INT, parent_of[rank], DEFAULT_TAG, 
							MPI_COMM_WORLD, &status);

			// receives number of rows & number of columns
			MPI_Recv(&rows[rank], 1, MPI_INT, parent_of[rank], DEFAULT_TAG, 
							MPI_COMM_WORLD, &status);

			if (rows[rank] != 0) {
				MPI_Recv(&columns[rank], 1, MPI_INT, parent_of[rank], DEFAULT_TAG, 
							MPI_COMM_WORLD, &status);
		//		cout << rank << " | " << rows[rank] << " | " << columns[rank] << endl;

				// create matrix
				int** piece = new int*[rows[rank] + EXTENSION];
				for (int i = 0 ; i < rows[rank] + EXTENSION; i++) {
					piece[i] = new int[columns[rank] + EXTENSION];
				}

				// receive his piece of the matrix, line by line
				for (int i = 0; i < rows[rank] + EXTENSION; i++) {
					MPI_Recv(&piece[i][0], columns[rank] + EXTENSION, MPI_INT, 
							parent_of[rank], DEFAULT_TAG, MPI_COMM_WORLD, &status);
				}

				if (is_leaf(rank, children)) {
					// process the matrix and send it back
					// int new_mat[rows[rank]][columns[rank]];
					int** new_mat = new int*[rows[rank]];
					for (int i = 0 ; i < rows[rank]; i++) {
						new_mat[i] = new int[columns[rank]];
					}

					if (filter == SOBEL) {
						apply_sobel_x(piece, rows[rank], columns[rank], 
										new_mat, max_pixel_value);
					}
					else if (filter == MEAN_REMOVAL) {
						apply_mean_removal(piece, rows[rank], columns[rank], 
											new_mat, max_pixel_value);
					}
					statistics[rank] += rows[rank];

					// send processed matrix back to parent
					for (int j = 0 ; j < rows[rank]; j++) {
						MPI_Send(&new_mat[j][0], columns[rank], MPI_INT, 
								parent_of[rank], DEFAULT_TAG, MPI_COMM_WORLD);
					}
				}
				// if is not leaf
				else {
					// split the matrix and send it to children
					int rows_to_send;
					int curr_line_to_send = 1;
					int ** processed_mat;
					int var;

					for (int child = 0; child < children[rank].size(); child++) {
						// if there are less lines than children
						if (rows[rank] < children[rank].size())
							// send each children only one line
							if (child + 1 <= rows[rank])
								rows_to_send = 1;
							// until all lines are sent
							else rows_to_send = 0;
						// if there are more lines than children
						else {
							rows_to_send = int (rows[rank] / children[rank].size());
							// add remainder if it's the last children
							if (child == children[rank].size() - 1) 
								 rows_to_send += rows[rank] % children[rank].size();
						}

						// send filter type to this child
						MPI_Send(&filter, 1, MPI_INT, children[rank][child], 
										DEFAULT_TAG, MPI_COMM_WORLD);

						// send max_value to this child
						MPI_Send(&max_pixel_value, 1, MPI_INT, children[rank][child], 
										DEFAULT_TAG, MPI_COMM_WORLD);

						// send number of rows to this child
						MPI_Send(&rows_to_send, 1, MPI_INT, children[rank][child], 
										DEFAULT_TAG, MPI_COMM_WORLD);
						// if there are lines to send, send the matrix
						if (rows_to_send != 0) {
							// send num of cols to all the child
							MPI_Send(&columns[rank], 1, MPI_INT, children[rank][child], 
										DEFAULT_TAG, MPI_COMM_WORLD);

							// send matrix row by row
							for (int k = 0 ; k < rows_to_send + EXTENSION; k++) {
								MPI_Send(&piece[curr_line_to_send - 1][0], columns[rank] + EXTENSION, 
										MPI_INT, children[rank][child], DEFAULT_TAG, MPI_COMM_WORLD);
								curr_line_to_send++;
							}
							curr_line_to_send -= EXTENSION;

							// receive back the processed matrix from this child
							int rows_to_recv = rows_to_send;

							// create matrix dimensions
							processed_mat = new int*[rows_to_recv];
							for (int i = 0 ; i < rows_to_recv; i++) {
								processed_mat[i] = new int[columns[rank]];
							}

							// receives processed lines from child and sends them to parent
							for (int l = 0; l < rows_to_recv; l++) {
								MPI_Recv(&processed_mat[l][0], columns[rank], MPI_INT, 
										children[rank][child], DEFAULT_TAG, MPI_COMM_WORLD, &status);
								MPI_Send(&processed_mat[l][0], columns[rank], MPI_INT, parent_of[rank], 
										DEFAULT_TAG, MPI_COMM_WORLD);
							}
						}
					}
				}
			}
		}

		int flag;
		int received_stats[num_workers];

		// receive termination flag
		MPI_Recv(&flag, 1, MPI_INT, parent_of[rank], DEFAULT_TAG, MPI_COMM_WORLD, &status);
		if (flag == TERMINATION_FLAG) {
			// if its not leaf
			if (children[rank].size() > 0) {
				for (int child = 0; child < children[rank].size(); child++) {
					MPI_Send(&flag, 1, MPI_INT, children[rank][child], 
							DEFAULT_TAG, MPI_COMM_WORLD);
					MPI_Recv(&received_stats, num_workers, MPI_INT, 
							children[rank][child], DEFAULT_TAG, MPI_COMM_WORLD, &status);

					// processes its statistics
					for (int i = 0 ; i < num_workers; i++)
						if (received_stats[i] > statistics[i]) statistics[i] = received_stats[i];
				}
				MPI_Send(&statistics, num_workers, MPI_INT, parent_of[rank], 
							DEFAULT_TAG, MPI_COMM_WORLD);

			}
			// if leaf
			else {
				MPI_Send(&statistics, num_workers, MPI_INT, parent_of[rank], 
							DEFAULT_TAG, MPI_COMM_WORLD);
			}
		}
	}
		
	// close program
	topologie.close();
	MPI_Finalize();
	return SUCCESS;
}