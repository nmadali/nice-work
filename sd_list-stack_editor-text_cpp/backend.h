#include <iostream>
#include "MyList.h"
#include "MyStack.h"

#ifndef BACKEND_H_
#define BACKEND_H_

class Backend {
	int *indexArray; // array to store the length for each word
	int numWords;
	int NMAXWORDS;
	MyList *clipboard; // retains the last CUT/COPY operation;
	MyList text;
	MyStack<Data> undostk;
	MyStack<Data> redostk;

public:
	// Constructor
	Backend();

	// Destructor
	~Backend();

	// validStart returneaza nodul la care se alfa urmatorul cuvant
	// valid de la indexul "start"
	int validStart(const int start);

	// validStop returneaza nodul la care se afla ultimul cuvant
	// valid inainte de indexul "stop"
	int validStop(const int stop);

	// Cuts all the words from the start to the stop index
	void Cut(const int start, const int stop);

	// Saves all the words from the start to the stop index to the clipboard
	void Copy(const int start, const int stop);

	// Pastes all the words in the clipboard at the start index
	void Paste(const int start);

	// Undoes the last operation
	void Undo(void);

	// Redoes the last undone operation
	void Redo(void);

	// Adds the string held by "word" to the text at position "index"
	void Add(const int index, const char *word);

	// Rebuilds and returns the whole text held by the backend
	char *GetText(void);
};

#endif // BACKEND_H_
