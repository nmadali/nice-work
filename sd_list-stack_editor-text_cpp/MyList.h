#include <iostream>
#include <string>
#include <string.h>

#ifndef MY_LIST_H_
#define MY_LIST_H_

using namespace std;

struct Node {
	string word;
	struct Node *next;
};

class MyList {
	int size;

public:
	Node *head, *sentinel;

	// constructor MyList
	MyList() {
		sentinel = new Node;
		sentinel->next = NULL;
		sentinel->word = "";

		head = sentinel;
		size = 0;
	}

	// destructor MyList
	~MyList() {
		Node *temp = head;
		Node *next;

		while (temp != NULL) {
			next = temp->next;
			delete temp;
			temp = next;
		}
	}

	void add(const int position, const string element) {
		Node *newNode = new Node;
		Node *it;
		if (head == sentinel) it = sentinel->next;
		else it = head;

		if (position <= 1) {
			addFirst(element);
			return;
		}

		if (position > size) {
			addLast(element);
			return;
		}

		// go to the requested position
		for (int i = 1; i < position - 1; i++) {
			if (it->next == sentinel) {
				if (sentinel->next != NULL) {
					it = sentinel->next;
				}
				else break;
			}
			else {
				it = it->next;
			}
		}

		// assign values for newNode
		newNode->word = element;
		if (newNode->word.length() != 0) {
			if (newNode->word[newNode->word.length() - 1] != ' ') {
				newNode->word.append(" ");
			}
		}
		newNode->next = it->next;

		// insert newNode in the list
		it->next = newNode;
		size++;
	}


	void addFirst(const string element) {
		Node *newNode = new Node;
		newNode->next = head;
		newNode->word = element;
		if (newNode->word.length() != 0) {
			if (newNode->word[newNode->word.length() - 1] != ' ') {
				newNode->word.append(" ");
			}
		}
		head = newNode;
		++size;
	}

	void addLast(const string element) {
		Node *newNode = new Node;
		newNode->next = NULL;
		newNode->word = element;
		if (newNode->word.length() != 0) {
			if (newNode->word[newNode->word.length() - 1] != ' ') {
				newNode->word.append(" ");
			}
		}

		// go to the last Node
		Node *it = head;
		while (it->next != NULL) {
			it = it->next;
		}
		it->next = newNode;

		if (head == sentinel) {
			head = sentinel->next;
		}
		++size;
	}

	string remove(int position) {
		Node * it = head;
		Node * prev;
		string removed;
		int count = 1;

		if (position == 1)
			return removeFirst();

		// goes to the requested position
		while (count != position) {
			if (it->next == sentinel)
				it = it->next;

			if (count == position - 1) {
				if (it == sentinel)
					prev = sentinel;
				else
					prev = it;
			}
			it = it->next;
			count++;
		}

		prev->next = it->next;
		removed = it->word;
		delete it;
		size--;
		return removed;
	}

	string removeFirst() {
		string removed = head->word;
		Node * temp = head;

		if (temp == sentinel) {
			temp = temp->next;
			removed = temp->word;
			sentinel->next = temp->next;
			delete temp;
			size--;
			return removed;
		}

		head = temp->next;
		removed = temp->word;
		delete temp;
		size--;
		return removed;
	}

	string removeLast() {
		string removed;
		Node *it = head;

		// go before the last element
		int i = size;
		while (i > 2) {
			if (it->next == sentinel) {
				it = sentinel->next;
				i--;
			}
			else {
				it = it->next;
				i--;
			}
		}

		removed = it->next->word;
		// delete the last element
		delete it->next;
		it->next = NULL;
		--size;
		return removed;
	}

	char* get_words() {
		string text;
		Node *it = head;

		while (it != NULL) {
			text.append(it->word);
			if (it->next == sentinel) {
					it = sentinel->next;
			}
			else {
				it = it->next;
			}
		}
		char * ctext = new char[text.length() + 1];
		strcpy(ctext, text.c_str());
		return ctext;
	}
};

#endif // MY_LIST_H_
