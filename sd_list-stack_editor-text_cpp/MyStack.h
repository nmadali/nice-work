#ifndef MYSTACK_H_
#define MYSTACK_H_

#include <iostream>
using namespace std;

class Data {
public:
	int position[500];
	string word[500];
	int size;
	char operation;

	Data(char operation = 'n') {
		size = 0;
		this->operation = operation;
	}

	Data(const Data & obj) {
		this->operation = obj.operation;
		this->size = obj.size;

		for(int i = 0; i < obj.size; i++) {
			this->position[i] = obj.position[i];
			this->word[i] = obj.word[i];
		}
	}

	void operator = (const Data & obj) {
		this->operation = obj.operation;
		this->size = obj.size;

		for (int i = 0; i < this->size; i++) {
			this->position[i] = obj.position[i];
			this->word[i] = obj.word[i];
		}
	}

	void addData(int position, string word) {
		this->position[size] = position;
		this->word[size] = word;
		size++;
	}
};

template <typename T, int MAXDIM = 500>
class MyStack
{
	T *stackBuffer;
	int size;

public:
	MyStack() {
		size = 0;
		stackBuffer = new T[MAXDIM];
	}

	~MyStack() {
		delete[] stackBuffer;
	}


	void push(T element) {
		if (size != MAXDIM) {
			stackBuffer[size] = element;
			size++;
		}
		else if (size == MAXDIM) {
			T *temp = new T[MAXDIM];
			for (int i = 0; i < size; i++) {
				temp[i] = stackBuffer[i];
			}
			delete[] stackBuffer;

			stackBuffer = new T[MAXDIM * 2];
			for (int i = 0; i < size; i++) {
				stackBuffer[i] = temp[i];
			}
			delete[] temp;

			stackBuffer[size] = element;
			size++;
		}
	}

	T pop() {
		if (size > 0) {
			size--;
			return stackBuffer[size];
		}
		else {
			struct Data newdata;
			return newdata;
		}
	}

	T peek() {
		return stackBuffer[size - 1];
	}

	bool empty() {
		return (size == 0);
	}

	int getSize() {
		return size;
	}
};

#endif // MYSTACK_H_
