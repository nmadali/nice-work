#include <iostream>
#include <string.h>
#include <string>
#include "backend.h"

Backend::Backend() {
	NMAXWORDS = 501;
	indexArray = new int[NMAXWORDS];
	for (int i = 1; i < NMAXWORDS; i++)
		indexArray[i] = 0;
	numWords = 0;
	clipboard = new MyList;
}

Backend::~Backend() {
	delete[] indexArray;
	delete clipboard;
}

void Backend::Cut(const int start, const int stop) {
	delete clipboard;
	clipboard = new MyList;

	int startWord = validStart(start);
	int stopWord = validStop(stop);

	Node *it = text.head;
	// go to the first word to be copied
	for (int i = 1; i < startWord; i++) {
		if (it->next == text.sentinel) {
			if (text.sentinel->next == NULL) {
				std::cout << GetText() << endl;
				return;
			}
			else {
				it = text.sentinel->next;
			}
		}
		else {
			if (it->next != NULL) {
				it = it->next;
			}
			else {
				std::cout << GetText() << endl;
				return;
			}
		}
	}

	// add words to be copied in clipboard (create a list with them)
	for (int i = startWord; i <= stopWord; ++i) {
		if (it == text.sentinel) {
			it = it->next;
			clipboard->addLast(it->word);
			continue;
		}

		clipboard->addLast(it->word);

		// move to the next word
		if (it->next != text.sentinel) {
			if (it->next == NULL)
				break;
			else {
				it = it->next;
			}
		}
		else {
			if (text.sentinel->next != NULL) {
				it = text.sentinel->next;
			}
			else
				break;
		}
	}

	// remove from indexArray
	Data newdata('c');
	string removed;
	int count = 0;

	for (int i = startWord; i <= stopWord; i++) {
		removed = text.remove(i - count);
		newdata.addData(i - count, removed);

		for (int j = i - count; j <= numWords; j++) {
			indexArray[j] = indexArray[j + 1];
		}
		numWords--;
		indexArray[numWords + 1] = 0;
		count++;
	}
	// update undo's stack
	undostk.push(newdata);

	std::cout << GetText() << endl;
}

void Backend::Copy(const int start, const int stop) {
	delete clipboard;
	clipboard = new MyList;

	int startWord = validStart(start);
	int stopWord = validStop(stop);

	Node *it = text.head;
	// go to the first word to be copied
	for (int i = 1; i < startWord; i++) {
		if (it->next == text.sentinel) {
			if (text.sentinel->next == NULL) {
				std::cout << GetText() << endl;
				return;
			}
			else {
				it = text.sentinel->next;
			}
		}
		else {
			if (it->next != NULL) {
				it = it->next;
			}
			else {
				std::cout << GetText() << endl;
				return;
			}
		}
	}

	// add words to be copied in clipboard (create a list with them)
	for (int i = startWord; i <= stopWord; ++i) {
		if (it == text.sentinel) {
			it = it->next;
			clipboard->addLast(it->word);
			continue;
		}

		clipboard->addLast(it->word);

		// move to the next word
		if (it->next != text.sentinel) {
			if (it->next == NULL)
				break;
			else {
				it = it->next;
			}
		}
		else {
			if (text.sentinel->next != NULL) {
				it = text.sentinel->next;
			}
			else
				break;
		}
	}
	std::cout << GetText() << endl;
}

void Backend::Paste(const int start) {
	int startWord = validStart(start);
	int iterNumber = 0;
	Node * it = clipboard->head;
	Data newdata('p');

	// iterate the clipboard
	while (it != NULL) {
		if (it->word != "") {
			// add the nodes from the clipboard into text
			text.add(startWord + iterNumber, it->word);
			newdata.addData(startWord + iterNumber, it->word);
			numWords++;
			// shift the index vector
			for (int i = startWord + iterNumber; i < numWords; i++) {
				indexArray[i + 1] = indexArray[i];
			}
			indexArray[startWord + iterNumber] = it->word.length();
		}

		iterNumber++;
		if (it->next == clipboard->sentinel) {
			it = clipboard->sentinel->next;
		}
		else {
			it = it->next;
		}
	}
	// update undo's stack
	undostk.push(newdata);

	std::cout << GetText() << endl;
}

void Backend::Undo(void) {
	Data newdata = undostk.pop();
	if (newdata.operation == 'a') {
		text.remove(newdata.position[0]);
		redostk.push(newdata);
	}

	if (newdata.operation == 'c') {
		for (int i = newdata.size - 1; i >= 0; i--) {
			text.add(newdata.position[i], newdata.word[i]);
		}
		redostk.push(newdata);
	}

	if (newdata.operation == 'p') {
		for (int i = newdata.size - 1; i >= 0; i--) {
			text.remove(newdata.position[i]);
		}
		redostk.push(newdata);
	}

	cout << GetText() << endl;
}

void Backend::Redo(void) {
	Data newdata = redostk.pop();

	if (newdata.operation == 'a') {
		text.add(newdata.position[0], newdata.word[0]);
	}

	if (newdata.operation == 'c') {
		for (int i = 0; i < newdata.size; i++) {
			text.remove(newdata.position[i]);
		}
	}

	if (newdata.operation == 'p') {
		for(int i = 0; i < newdata.size; i++) {
			text.add(newdata.position[i], newdata.word[i]);
		}
	}

	cout << GetText() << endl;
}

void Backend::Add(const int index, const char *word) {
	int position = validStart(index);
	text.add(position, word);

	// update undo's stack
	Data newdata('a');
	newdata.addData(position, word);
	undostk.push(newdata);

	// update the indexes vector
	numWords++;
	for (int i = position; i <= numWords - 1; i++) {
		indexArray[i + 1] = indexArray[i];
	}
	indexArray[position] = strlen(word) + 1; // +1 for the final " ".

	std::cout << GetText() << endl;
}

char * Backend::GetText(void) {
	return text.get_words();
}

int Backend::validStart(const int start) {
	int startNode = 1, startindex = 0;

	if (start <= 0) return 1;
	if (indexArray[startNode] == 0) return 1;

	int sum = 0;
	for (int i = 0; i <= numWords; i++)
		sum += indexArray[i];
	if (sum < start) return numWords + 1;

	while (startindex < start) {
		startindex += indexArray[startNode];
		startNode++;
	}
	return startNode;
}

int Backend::validStop(const int stop) {
	int stopNode = 1, stopindex = 0;

	int sum = 0;
	for (int i = 0; i <= numWords; i++)
		sum += indexArray[i];
	if (sum <= stop) return numWords;

	while (stopindex < stop) {
		if (indexArray[stopNode] == 0)
			break;
		stopindex += indexArray[stopNode];
		if (stopindex - 1 < stop)
			stopNode++;
	}
	return (stopNode - 1);
}
