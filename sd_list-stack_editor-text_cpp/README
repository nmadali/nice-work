------------------------------------------
			STRUCTURI DE DATE	
				- TEMA #2 - 	
		
		  NAUM MADALIN CONSTANTIN
				   314CA
------------------------------------------


	Pentru implementarea tuturor functionalitatilor clasei backend a fost 
nevoie in primul rand de implementarea unei liste (MyList) si a unei 
stive (MyStack).
	
	MyList este o lista simplu inlantuita, facuta special pentru a functiona
pentru tipul string, deoarece obiectivul temei (implementarea unui editor
text) a fost foarte specifica, nefiind necesar/util un caracter generic al
listei. 

	 MyList contine operatiile de baza:
		- void addLast(const string element)
			Adauga un element la final.
		- void addFirst(const string element)
			Adauga un element la inceput.
		- void add(const int position, const string element)
			Adauga un element la pozitia position
		- string remove(int position)
			Sterge elementul de la pozitia position.
		- string removeFirst()
			Sterge primul element.
		- string removeLast()
			Sterge ultimul element.
		- char* get_words() 
			Returneaza textul format de lista.

	Pe langa acestea, au fost implementate si constructor, assignment
operator si copy-constructor.

	MyStack este o stiva generica ce suporta operatiile de baza: push, pop, 
peek, empty si getSize.





-------------------------------------------------------------------------

	Clasa backend este destinata implementarii partii de back-end a unui
editor text ce are ca unitate de baza cuvantul. Aceasta a fost definita 
pentru a suporta operatiile de baza prezente la editoarele text: add, 
copy, paste, cut, undo si redo.
	
	In continuare va fi explicata fiecare in parte:

-------------------------------------------------------------------------
1.		void Backend::Add(const int index, const char *word);
-------------------------------------------------------------------------

	Functia Add primeste ca parametri un index (indexul din text la care trebuie
sa se adauge cuvantul) si un const char *word - cuvantul ce trebuie adaugat.

	Implementare:
-----------------	
->	Pentru inceput, este apelata functia validStart, care primeste ca parametru
valoarea index, reprezentand indexul din text la care trebuie sa se adauge. 
	Functia validStart returneaza pozitia din lista (nodul) la care
trebuie sa se adauge cuvantul. Pentru implementarea acestuia a fost necesara
crearea vectorului indexArray, care retine lungimile cuvintelor ce se afla
la un anumit moment in lista si care se actualizeaza imediat dupa fiecare 
modificare in lista.

->	Este adaugat cuvantul dorit la nodul indicat de functia validStart, prin 
apelul functiei add a listei.

->	Este actualizata stiva pentru operatia undo (undostk).

->	Este actualizat vectorul ce retine lungimile cuvintelor prezente in lista.

->	Este apelata functia GetText, care returneaza starea curenta a textului.

-------------------------------------------------------------------------
2.	void Backend::Copy(const int start, const int stop);
-------------------------------------------------------------------------

	Functia Copy primeste ca parametri doi indecsi - inceput si final - si,
apeland functiile validStart si validStop, obtine nodurile intre care trebuie
sa se faca operatia de copiere.

	Implementare:
-----------------
->	Pentru inceput este important sa fie prezentata implementarea clipboardului. 
	Clipboardul este utilizat pentru a stoca modificarile pe care le produc 
operatiile Copy, Paste si Cut in lista initiala. Practic, clipboardul a fost
implementat ca un pointer la un element de tipul lista, si este realocat la 
inceputul oricarei dintre operatiile: Cut, Copy sau Paste.
	Astfel, primul lucru ce se realizeaza la apelul functiei Copy este
golirea clipboardului si realocarea sa.

->	Sunt apelate functiile validStart si validStop pentru a se obtine nodurile
intre care se lucreaza.

->	Folosind un iterator de tip Node*, se parcurge lista pana la primul nod la
care trebuie facute modificari.

->	Se adauga in clipboard cuvintele ce trebuie copiate. Practic, se creeaza 
un duplicat al acelei parti a listei denumita text. 

->	In acest moment operatia Copy este finalizata, ramanand doar sa se apeleze
GetText.


-------------------------------------------------------------------------
3.	void Backend::Paste(const int start);
-------------------------------------------------------------------------

	Functia Paste primeste ca parametru indexul de start, si pe baza lui 
determina nodul de la care incepe operatia Paste.

	Implementare:
-----------------
->	Se determina nodul de start, folosind functia validStart.

->	Se itereaza clipboardul. La fiecare iteratie:
	-> Se adauga un nod, din clipboard in text (duplicare).

	-> Se shifteaza vectorul de indecsi.
	
->	La final se actualizeaza stiva lui undo si se apeleaza GetText().


-------------------------------------------------------------------------
4.	void Backend::Cut(const int start, const int stop);
-------------------------------------------------------------------------

	Functia Cut primeste ca parametri indecsii de start si de final si 
face operatia de taiere intre acele cuvinte.

	Implementare:
-----------------
->	Se sterge (dezaloca) si realoca clipboardul.	

->	Se calculeaza nodul de start si cel de final folosind functiile
validStart si validStop.

->	Se itereaza pana la primul cuvant ce trebuie taiat (copiat in clipboard).

->	Se copiaza, incepand de acolo, cate noduri trebuie, in clipboard.

->	Se actualizeaza vectorul de indecsi (cel care retine lungimile cuvintelor).

->	Se actualizeaza stiva pentru undo.

->	Se apeleaza GetText pentru a afisa noua stare a listei si se iese 
din functie.


-------------------------------------------------------------------------
5.	void Backend::Undo(void);
-------------------------------------------------------------------------

	Pentru implementarea operatiilor Undo si Redo a fost aleasa crearea
unei alte structuri (Data), care sa retina informatii despre operatiile 
efectuate, precum nodul la care s-a facut operatia, cuvantul pe care s-a facut
operatia si tipul operatiei. Astfel, stiva de undo, care a fost
implementata generic, este apelata pentru a functiona pe elemente de tipul
Data (MyStack<Data>).

	Implementare:
-----------------
->	Tot ceea ce trebuie sa faca operatia Undo este sa scoata un element din
stiva undostk si sa reactioneze la informatiile pe care acesta le contine

	-> Daca operatia a fost add, atunci trebuie sa se faca stergerea si sa 
		se notifice acest lucru in stiva pentru Redo.

	-> Daca operatia a fost cut (care presupune stergerea unor noduri), operatia
		inversa este adaugarea. Se realizeaza aceasta operatie, apoi se 
		actualizeaza stiva pentru Redo.

	-> Daca operatia a fost Paste (care presupune adaugarea unor noduri), 
		operatia inversa este stergerea. Se sterge elementul/elementele in 
		cauza si se actualizeaza stiva Redo.

->	Se apeleaza GetText()


-------------------------------------------------------------------------
6.	void Backend::Redo(void);
-------------------------------------------------------------------------

	Functia Redo este similara functiei Undo. Se foloseste si ea de o stiva in
care se retin informatii despre operatia efectuata.

	Implementare:
-----------------
->	Scoate un element din stiva redostk si se comporta asa cum indica
informatiile din stiva.
	
	-> Daca operatia a fost Add, atunci trebuie sa se adauge elementul la 
		pozitia data.

	-> Daca operatia a fost Cut, se face stergerea elementelor indicate.

	-> Daca operatia a fost Paste, se adauga elementele la pozitiile cerute.

->	Se apeleaza functia GetText si se iese din Undo.
-------------------------------------------------------------------------
